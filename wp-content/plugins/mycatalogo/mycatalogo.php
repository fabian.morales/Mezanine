<?php
/*
Plugin Name: Catalogo de productos y carrito
Description: Plugin para mostrar el catalogo y el carrito de compras
*/

    /*add_filter('wp_nav_menu_items', 'add_login_logout_link', 10, 2);
    function add_login_logout_link($items, $args) {
        ob_start();
        wp_loginout('index.php');
        $loginoutlink = ob_get_contents();
        ob_end_clean();
        $items .= '<li>'. $loginoutlink .'</li>';
        return $items;
    }*/
    add_action( 'wp_ajax_carrito_Add', 'blel' ); // Write our JS below here

    function blel() {
	echo "lel";
        die();
    }
    
    function mycatalogo_shortcode( $atts ){
        include_once ABSPATH."catalogo/myCore/autoload.php";
        
        if (is_array($atts) && sizeof($atts)){
            if (array_key_exists("formato", $atts)){
                myView::establecerFormato($atts["formato"]);
            }
            
            if (array_key_exists("prefijo", $atts)){
                myView::establecerPreUrl($atts["prefijo"]);
            }
        }

        wp_enqueue_style("mycatalogo_foundation", myView::asset('assets/css/foundation/foundation-grid.css'));
        wp_enqueue_style("mycatalogo_foundation_font", myView::asset('assets/css/foundation/fonts/foundation-icons.css'));
        wp_enqueue_style("mycatalogo_feather", myView::asset('assets/js/featherlight/featherlight.css'));
        wp_enqueue_style("mycatalogo_alerts", myView::asset('assets/js/jquery.alerts/jquery.alerts.css'));
        wp_enqueue_style("mycatalogo_base", myView::asset('assets/css/estilos.css'));
        wp_enqueue_style("mycatalogo_raws", myView::asset('assets/css/raws.css'));
        wp_enqueue_script("mycatalogo_feather", myView::asset('assets/js/featherlight/featherlight.js'));
        wp_enqueue_script("mycatalogo_alerts", myView::asset('assets/js/jquery.alerts/jquery.alerts.js'));
        wp_enqueue_script("mycatalogo_jqueryui", myView::asset('assets/js/jquery-ui/jquery-ui.js'));
        wp_enqueue_script("mycatalogo_base", myView::asset('assets/js/catalogo.js'));
        
        $req = myApp::getRequest();
        $c = myApp::getController($req->getVar('controller', 'catalogo'));
        return $c->retornar($req->getVar('task'));
    }
    
    function mycarrito_shortcode( $atts ){
        include_once ABSPATH."catalogo/myCore/autoload.php";
        
        if (is_array($atts) && sizeof($atts)){
            if (array_key_exists("formato", $atts)){
                myView::establecerFormato($atts["formato"]);
            }
            
            if (array_key_exists("prefijo", $atts)){
                myView::establecerPreUrl($atts["prefijo"]);
            }
        }

        wp_enqueue_style("mycatalogo_foundation", myView::asset('assets/css/foundation/foundation-grid.css'));
        wp_enqueue_style("mycatalogo_foundation_font", myView::asset('assets/css/foundation/fonts/foundation-icons.css'));
        wp_enqueue_style("mycatalogo_feather", myView::asset('assets/js/featherlight/featherlight.css'));
        wp_enqueue_style("mycatalogo_base", myView::asset('assets/css/estilos.css'));
        wp_enqueue_style("mycatalogo_raws", myView::asset('assets/css/raws.css'));
        
        $c = myApp::getController('carrito');
        return $c->retornar('mostrarTotalesModulo');
    }
    
    add_shortcode( 'mycatalogo', 'mycatalogo_shortcode' );
    add_shortcode( 'mycarrito', 'mycarrito_shortcode' );