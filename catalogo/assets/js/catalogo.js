(function (window, $) {
    var $posThumb= 0; 
    
    function hookModuloCarrito() {
        $("#modCarrito a.boton-cantidad").click(function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                method: 'get',
                success: function (res) {
                    $("#modCarrito").fadeOut(200);
                    $("#modCarrito").html(res).fadeIn(200);
                    hookModuloCarrito();
                }
            });
        });
    }
    
    function hookListaItems(){        
        $("a[rel='lnkQuitarItem']").click(function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                method: 'get',
                data: {
                    format: 'raw'
                },
                success: function(res){
                    window.location.reload();
                }
            });
        });
        
        $("a[rel='lnkActualizarItem']").click(function(e) {
            e.preventDefault();
            var $dataReg = $(this).attr("data-reg");
            var $cantidad = $($dataReg).val();
            $.ajax({
                url: $(this).attr("href"),
                method: 'get',
                data: {
                    format: 'raw',
                    cantidad: $cantidad
                },
                success: function(res){
                    window.location.reload();
                }
            });
        });
    }

    $(document).ready(function () {
        hookModuloCarrito();
        hookListaItems();
        
        $("img[rel='thumbImagen']").click(function (e) {
            e.preventDefault();
            $(".imgThumbActiva").removeClass("imgThumbActiva");
            $(this).addClass("imgThumbActiva");
            $("#idProducto").attr("href", $(this).attr("data-img"));
            $("#idProducto > img").attr("src", $(this).attr("src"));
        });

        $("a[rel='lnkAgregarProd']").click(function (e) {
            e.preventDefault();
            $cantidad = $("#cantidad").val();
            $idExt = $("#idExt").val();
            $idProd = $("#id").val();

            $.ajax({
                url: $(this).attr("href"),
                method: 'get',
                data: { id_producto: $idProd, id_ext: $idExt, cantidad: $cantidad, format: 'raw' },
                success: function (res) {
                    /*$("#modCarrito").fadeOut(200);
                    $("#modCarrito").html(res).fadeIn(200);
                    hookModuloCarrito();*/
                    window.location.reload();
                }
            });
        });
        
        $("a[rel='lnk_add_home']").click(function (e) {
            e.preventDefault();            

            $.ajax({
                url: $(this).attr("href"),
                method: 'get',
                data: { format: 'raw', action:'carrito_Add' },
                success: function (res) {
                    /*$("#modCarrito").fadeOut(200);
                    $("#modCarrito").html(res).fadeIn(200);
                    hookModuloCarrito();*/
                    window.location.reload();
                }
            });
        });
        
        $("a.lnkColor").click(function(e) {
            e.preventDefault();
            var $idExt = $(this).attr("data-ext");
            $("#idExt").val($idExt);
        });
        
        $("select#talla").change(function(e) {
            e.preventDefault();
            var $idExt = $(this).val();
            $("#idExt").val($idExt);
        });        
        
        $(".modulo_carrito").draggable();
        
        var $cnt = $("ul#thumbs li").size();
        var $w = $("ul#thumbs li:first-child").width();
        
        $("ul#thumbs").css("width", $cnt * ($w + 10));
        
        $(".img.siguiente").click(function(e) {
            e.preventDefault();            

            var $w1 =  $("ul#thumbs").width();
            var $w2 =  $("#lista_thumbs").width();
            var $offset = $w1 - $w2;

            if (Math.abs($posThumb) < $offset){
                $posThumb -= 80;
                $("ul#thumbs").css("left", $posThumb);
            }
        });
        
        $(".img.anterior").click(function(e) {
            e.preventDefault();
            
            if ($posThumb < 0){
                $posThumb += 80;
                $("ul#thumbs").css("left", $posThumb);
            }
        });
        
        $("#formLogin").submit(function(e) {
            e.preventDefault();
            
            if ($("usuario_login").val() === ""){
                jAlert('Debe ingresar el nombre de usuario', 'Aceptar');
                return;
            }
            
            if ($("clave_login").val() === ""){
                jAlert('Debe ingresar la clave', 'Aceptar');
                return;
            }
            
            $.ajax({
                url: $("#formLogin").attr("action"),
                method: 'post',
                data: $("#formLogin").serialize(),
                dataType: 'json'
            })
            .success(function($datos){
                if ($datos.ok === 1){
                    window.location.href = $datos.redirect;
                }
                else{
                    jAlert($datos.error, 'Aceptar');                    
                }
            });
        });
        
        $("#formRegistro").submit(function(e) {
            e.preventDefault();
            
            if ($("usuario_regitro").val() === ""){
                jAlert("Debe ingresar el nombre de usuario", 'Aceptar');
                return;
            }
            
            if ($("email_regitro").val() === ""){
                jAlert("Debe ingresar la direccion de correo", 'Aceptar');
                return;
            }
            
            if ($("nombre_regitro").val() === ""){
                jAlert("Debe ingresar su nombre", 'Aceptar');
                return;
            }
            
            if ($("apellido_regitro").val() === ""){
                jAlert("Debe ingresar su apellido", 'Aceptar');
                return;
            }
            
            if ($("clave_regitro").val() === ""){
                jAlert("Debe ingresar la clave", 'Aceptar');
                return;
            }
            
            $.ajax({
                url: $("#formRegistro").attr("action"),
                method: 'post',
                data: $("#formRegistro").serialize(),
                dataType: 'json'
            })
            .success(function($datos){
                if ($datos.ok === 1){
                    jAlert("Se ha registrado satisfactoriamente, recibirá un correo de confirmación, tenga por favor en cuenta la carpeta de Spam", 'Aceptar', function() { window.location.href = $datos.redirect; });
                }
                else{
                    jAlert($datos.error);
                }
            });
        });
        
        $("#siguiente").one("click", function(e) {
            e.preventDefault();
            $.ajax({
                url: $(this).attr("href"),
                method: 'get'
            })
            .success(function($datos){
                jAlert("Su pedido se ha enviado", 'Aceptar', function() { window.location.href = 'index.php'; });
            })
            .error(function(jqXHR, textStatus) {
                jAlert(jqXHR.responseJSON.error.message, 'Aceptar');
            });
        });
        
        $("#lnkMas").click(function(e) {
            e.preventDefault();
            var $cnt = parseInt($("#cantidad").val());
            $cnt++;
            $("#cantidad").val($cnt);
        });
        
        $("#lnkMenos").click(function(e) {
            e.preventDefault();
            var $cnt = parseInt($("#cantidad").val());
            if ($cnt > 1){
                $cnt--;
                $("#cantidad").val($cnt);
            }
        });
        
        $("input[rel='cantidad_carrito']").keydown(function (e){
            if(e.keyCode === 13){
                e.preventDefault();
                var $dataUrl = $(this).attr("data-url");
                var $cantidad = $(this).val();
                $.ajax({
                    url: $dataUrl,
                    method: 'get',
                    data: {
                        format: 'raw',
                        cantidad: $cantidad
                    },
                    success: function(res){
                        window.location.reload();
                    }
                });
            }
        });
        
        /*var pressTimer;
        $("a.link_carrito_flotante").mouseup(function(){
            clearTimeout(pressTimer);            
            return false;
        }).mousedown(function(){
            pressTimer = window.setTimeout(function() { $("#contenido_carrito_flotante").toggle(); },1000);
            return false; 
        });*/
        
        $("a.link_carrito_flotante").click(function(e){
            e.preventDefault();
            $("#contenido_carrito_flotante").toggle();
        });
    });
})(window, jQuery);