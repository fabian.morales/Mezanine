<?php

abstract class myAdminController extends myController{
    public function ejecutar($tarea = "index", $args = []){
        if (!(is_user_logged_in() && is_super_admin())){
            myApp::mostrarMensaje("Acceso denegado", "error");
            myApp::redirect('wp-login.php', '', true);
            return "";
        }
        
        /*$app = JFactory::getApplication();
        if (!$app->isAdmin()){
            myApp::mostrarMensaje("Esta sección debe accederse desde el administrador", "error");
            return "";
        }*/
        
        parent::ejecutar($tarea, $args);
    }
    
    public function index() { }
}

