<?php

use \Illuminate\Database\Capsule\Manager as Capsule;

class adminPedidoController extends myAdminController{
    public function __construct() {
        $doc = myApp::getDocumento();
        $doc->addEstilo('assets/js/featherlight/featherlight.css');
        $doc->addScript('assets/js/featherlight/featherlight.js');
    }
    
    public function index(){
        $pedidos = Pedido::with(["usuario.nombre", "usuario.apellido"])->paginate(20);
        return myView::render("admin.pedido.index", ["pedidos" => $pedidos]);
    }
    
    public function detallePedido(){
        $idPedido = myApp::getRequest()->getVar("id");
        $pedido = Pedido::where("id", $idPedido)->with(["usuario.nombre", "usuario.apellido", "detalle.extension.producto", "detalle.extension.talla", "detalle.extension.color"])->first();
        return myView::render("admin.pedido.detalle", ["pedido" => $pedido]);
    }
}
