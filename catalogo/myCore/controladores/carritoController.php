<?php

class carritoController extends myController{
    function obtenerIdSesion(){
        $idSesion = mySession::get("id_sesion");
               
        if (empty($idSesion)){
            $idSesion = uniqid();
            mySession::set("id_sesion", $idSesion);            
        }
        return $idSesion;
    }

    function guardarIdSesion($id){
        mySession::set("id_sesion", $id);
    }

    public function index(){
        $idSesion = $this->obtenerIdSesion();
        $url = myApp::getUrlRoot();
        $carrito = Carrito::where("id_sesion", $idSesion)->with(["producto.imagenes", "extension.talla", "extension.color"])->get();
        $totales = Carrito::totales($idSesion);
        $myCfg = new myConfig();
        $logueado = myApp::estaLogueado() ? "S" : "N";
        return myView::render("carrito.index", ["urlImg" => myApp::urlImg(), "carrito" => $carrito, "totales" => $totales, "envio" => $myCfg->gastosEnvio, "logueado" => $logueado, "url" => $url]);
    }

    public function listarCarrito(){
        $idSesion = $this->obtenerIdSesion();
        $url = myApp::getUrlRoot();
        $carrito = Carrito::where("id_sesion", $idSesion)->with(["producto.imagenes", "extension.talla", "extension.color"])->get();
        $totales = Carrito::totales($idSesion);
        $myCfg = new myConfig();
        return myView::render("carrito.lista_items", ["urlImg" => myApp::urlImg(), "carrito" => $carrito, "totales" => $totales, "envio" => $myCfg->gastosEnvio, "url" => $url]);
    }

    public function agregarProducto(){
        $idSesion = $this->obtenerIdSesion();
        $request = myApp::getRequest();
        $idProducto = $request->getVar("id_producto");
        $idExt = $request->getVar("id_ext");
        $cantidad = $request->getVar("cantidad", 0, "int");
        $ext = Extension::find($idExt);

        if (sizeof($ext)){
            $reg = Carrito::where("id_sesion", $idSesion)->where("id_ext", $idExt)->first();            
            if (!sizeof($reg)){
                $reg = new Carrito();
                $reg->id_sesion = $idSesion;
                $reg->id_ext = $idExt;
                $reg->id_referencia = $idProducto;
                $reg->cantidad = 0;
                $reg->fecha = date('Y-m-d H:i:s');

                /*$user = JFactory::getUser();
                if ($user->id){
                    $reg->id_usuario = $user->id;
                }*/
            }

            $reg->cantidad += (int)$cantidad;
            $reg->save();
        }

        return $this->mostrarTotalesModulo();
    }
    
    public function actualizarItem(){
        $id = myApp::getRequest()->getVar("id");        
        $item = Carrito::where("id_sesion", $this->obtenerIdSesion())->where("id", $id)->first();
        if (sizeof($item)){
            $item->cantidad = (int)myApp::getRequest()->getVar("cantidad");
            $item->save();
            Carrito::where("id_sesion", $this->idSesion)->where("cantidad", "<=", 0)->delete();
        }
        
        //return $this->listarCarrito();
        return "";
    }
    
    public function quitarItem(){
        $id = myApp::getRequest()->getVar("id");        
        Carrito::where("id_sesion", $this->obtenerIdSesion())->where("id", $id)->delete();
        //return $this->listarCarrito();
        return "";
    }

    public function mostrarTotalesModulo(){
        $doc = myApp::getDocumento();
        $doc->addScript(myApp::getUrlRoot()."myCore/js/catalogo.js");
        $idSesion = $this->obtenerIdSesion();
        $carrito = Carrito::where("id_sesion", $idSesion)
                ->with(["extension.producto.imagenes", "extension.talla", "extension.color"])
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();
        $totales = Carrito::totales($idSesion);
        $myCfg = new myConfig();
        return myView::render("carrito.modulo", ["urlImg" => myApp::urlImg(), "carrito" => $carrito, "totales" => $totales, "envio" => $myCfg->gastosEnvio]);
    }
    
    public function enviarPedido(){
        $db = myApp::getEloquent();
        $pdo = $db::connection()->getPdo();
        $pdo->beginTransaction();
        $cfg = new myConfig();
        $iva = $cfg->porcIva;        
        try
        {
            $idSesion = $this->obtenerIdSesion();
            $carrito = Carrito::where("id_sesion", $idSesion)->with(["extension.producto"])->get();
            $totales = Carrito::totales($idSesion);
            
            if (!sizeof($carrito)){
                throw new \Exception('No hay productos en su carrito');
            }            
            $usuario = myApp::obtenerUsuario();
            
            $pedido = new Pedido();
            $pedido->id_user = $usuario->ID;
            $pedido->direccion = "";
            $pedido->num_items = (int)$totales["cantidad_total"];
            $pedido->cargo_envio = 0;
            $pedido->valor_items = (float)$totales["pesos_total"];
            $pedido->valor_total = (float)$totales["pesos_total"];
            $pedido->porc_iva = (float)$iva;
            $pedido->valor_iva = 0;
            $pedido->fecha = date('Y-m-d H:i:s');
            $pedido->estado = 'N';
            $pedido->cod_trans = '';
        
            if ($pedido->save()){
                
                foreach ($carrito as $c){
                    $detalle = new DetallePedido();
                    $detalle->id_pedido = $pedido->id;
                    $detalle->id_referencia = $c->extension->producto->id;
                    $detalle->id_ext = $c->extension->id;
                    $detalle->cantidad = $c->cantidad;
                    $detalle->valor = $c->extension->producto->valor_base * $c->cantidad;
                    $detalle->porc_iva = $iva;
                    $detalle->valor_iva = $detalle->valor - ($detalle->valor / (1 + $iva / 100));
                    
                    if (!$detalle->save()){
                        throw new \Exception('No se pudo guardar el detalle de la cotizacion');
                        //myApp::redirect("index.php", "No se pudo guardar el detalle del pedido");
                    }
                }
                
                $carrito = Carrito::where("id_sesion", $idSesion)->delete();
                $pdo->commit();
                mySession::clear("pedido");
                
                $mensaje = $this->generarCorreo($pedido->id);
                //myApp::enviarCorreo(["desarrollo@encubo.ws", $usuario->user_email], "Cotización nueva - Mezanine", $mensaje);
                myApp::enviarCorreo([$usuario->user_email], "Cotización nueva - Mezanine", $mensaje);
                myApp::enviarCorreo(["ventas@mezanine.com.co"], "Cotización nueva - Mezanine", $mensaje);
            }
            else{
                throw new \Exception('No se pudo crear la cotizacion');
                //myApp::redirect("index.php", "No se crear el pedido");
            }
        }catch(Exception $e){            
            $pdo->rollback();
            throw $e;
            //myApp::redirect("index.php", "No se pudo realizar el pedido");
        }
    }
    
    public function generarCorreo($idPedido=""){
        if (empty($idPedido)){
            $idPedido = myApp::getRequest()->getVar("id");
        }
        
        $usuario = myApp::obtenerUsuario();
        $pedido = Pedido::where("id", $idPedido)->with(["detalle.extension.producto", "detalle.extension.talla", "detalle.extension.color"])->first();
        return myView::render("carrito.correo_pedido", ["pedido" => $pedido, "url" => myApp::getUrlRoot(), "asunto" => "Pedido nuevo - Mezanine", "usuario" => $usuario]);
    }
    
    public function usuario(){
        $usuario = myApp::obtenerUsuario();
        print_r($usuario);
    }
}